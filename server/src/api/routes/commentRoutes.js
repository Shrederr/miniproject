import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
      // notify a user if  someone (not himself) liked his post
        req.io.to(reaction.comment.userId).emit('like', 'Your comment was liked!');
      }
      return res.send(reaction);
    })
    .catch(next));
router.put('/:id', (req, res) => {
  try {
    res.data = commentService.updateData(req);
    res.send(res.data);
  } catch (error) {
    res.status(400).send({
      error: true,
      message: error.message
    });
  }
});

router.delete('/:id', (req, res) => {
  try {
    res.data = commentService.deletePost(req);
    res.send(res.data);
  } catch (error) {
    res.status(400).send({
      error: true,
      message: error.message
    });
  }
});

export default router;
