import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const updatePostById = (post, text) => postReactionRepository.updateById(post, text);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });
  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const updateData = async req => {
  const post = await postRepository.getPostById(req.body.postId);
  if (post.dataValues.userId === req.user.dataValues.id) {
    await postRepository.updateById(req.body.postId, { body: req.body.text });
  }
};

export const deletePost = async req => {
  const post = await postRepository.getPostById(req.body.postId);
  if (post.dataValues.userId === req.user.dataValues.id) {
    await postRepository.deleteById(req.body.postId);
  }
};
