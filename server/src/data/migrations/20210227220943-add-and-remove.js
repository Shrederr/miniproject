module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('commentReactions', 'postId', { transaction }),
      queryInterface.addColumn('commentReactions', 'commentId', {
        type: Sequelize.UUID,
        references: {
          model: 'comments',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('commentReactions', 'commentId', { transaction }),
      queryInterface.addColumn('commentReactions', 'postId', {
        type: Sequelize.UUID,
        references: {
          model: 'posts',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction })
    ]))
};
