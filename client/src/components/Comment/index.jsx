import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Card, Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import styles from './styles.module.scss';
import * as actionEditor from '../editorModal/actionEditor';
import { deleteCommentById } from '../../containers/Thread/actions';

const actions = {
  deleteCommentById
};
const Comment = ({ comment, likeComment, dislikeComment }) => {
  const { body, createdAt, user, id } = comment;
  const dispatch = useDispatch();
  const editor = useSelector(state => state.editor);
  function openEditor() {
    editor.itsComment = true;
    dispatch(actionEditor.switchEditor({
      isOpen: true,
      id
    }));
  }
  function deleteComment() {
    dispatch(actions.deleteCommentById(id));
  }
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <Card.Content extra>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
            <Icon name="thumbs up" />
            {comment.likeCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
            <Icon name="thumbs down" />
            {comment.likeCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => openEditor(id)}>
            <Icon name="setting" />
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deleteComment(id)}>
            <Icon name="delete" />
          </Label>
        </Card.Content>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  dislikeComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired
};

export default Comment;
