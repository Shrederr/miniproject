const initialState = {
  editMessage: '',
  mutableMessage: {
    isOpen: false,
    id: ''
  },
  itsComment: false
};

export default function editorReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_MESSAGE':
      return {
        ...state,
        message: action.payload
      };
    case 'SWITCH_EDITOR':
      return {
        ...state,
        mutableMessage: {
          isOpen: action.payload.isOpen,
          id: action.payload.id
        }
      };
    default:
      return state;
  }
}

