import React, { useState } from 'react';
import './editorModal.css';
import { useDispatch, useSelector } from 'react-redux';
import { editPost, editComment } from 'src/containers/Thread/actions';
import * as actionEditor from './actionEditor';

const EditModal = () => {
  const [newEditMessage, setNewEditMessage] = useState('');
  const dispatch = useDispatch();
  const editor = useSelector(state => state.editor);
  function closeEditor() {
    editor.itsComment = false;
    dispatch(actionEditor.switchEditor({
      isOpen: false,
      id: ''
    }));
    setNewEditMessage('');
  }

  function sendEditMessage() {
    if (editor.itsComment) {
      dispatch(editComment(editor.mutableMessage.id, newEditMessage));
    } else {
      dispatch(editPost(editor.mutableMessage.id, newEditMessage));
    }
    closeEditor();
  }

  function changeNewMessage(e) {
    setNewEditMessage(e.target.value);
  }

  let classList = 'editor-wrapper closeEditor';

  if (editor.mutableMessage.isOpen) {
    classList += ' openEditor';
  }

  return (
    <div>
      <div className={classList}>
        Messages Editor
        <textarea
          className="message-fill"
          id="textEditData"
          value={newEditMessage}
          onChange={changeNewMessage}
        />
        <div>
          <input type="button" id="sendEditMessage" onClick={sendEditMessage} value="Ok" />
          <input type="button" id="closeEditWindow" value="Cancel" onClick={closeEditor} />
        </div>
      </div>
    </div>
  );
};
export default EditModal;
