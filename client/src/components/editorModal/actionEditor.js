export function setMessage(message) {
  return {
    type: 'SET_MESSAGE',
    payload: message
  };
}

export function sendEditMessage(newMessage, id) {
  return {
    type: 'SEND_EDIT_MESSAGE',
    payload: {
      messageText: newMessage,
      id
    }
  };
}

export function switchEditor(status) {
  return {
    type: 'SWITCH_EDITOR',
    payload: status
  };
}
